package sn.esp.inscription.repository;

import sn.esp.inscription.domain.AnneeUniversitaire;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the AnneeUniversitaire entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AnneeUniversitaireRepository extends JpaRepository<AnneeUniversitaire, Long> {
}
