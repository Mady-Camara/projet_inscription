package sn.esp.inscription.repository.search;

import sn.esp.inscription.domain.Medecin;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Medecin} entity.
 */
public interface MedecinSearchRepository extends ElasticsearchRepository<Medecin, Long> {
}
