package sn.esp.inscription.repository.search;

import sn.esp.inscription.domain.Formation;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Formation} entity.
 */
public interface FormationSearchRepository extends ElasticsearchRepository<Formation, Long> {
}
