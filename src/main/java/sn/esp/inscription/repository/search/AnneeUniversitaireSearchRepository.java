package sn.esp.inscription.repository.search;

import sn.esp.inscription.domain.AnneeUniversitaire;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link AnneeUniversitaire} entity.
 */
public interface AnneeUniversitaireSearchRepository extends ElasticsearchRepository<AnneeUniversitaire, Long> {
}
