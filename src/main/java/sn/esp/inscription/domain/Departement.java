package sn.esp.inscription.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import sn.esp.inscription.domain.enumeration.EnumTypeDept;

/**
 * A Departement.
 */
@Entity
@Table(name = "departement")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "departement")
public class Departement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(min = 2)
    @Column(name = "code_dept", nullable = false, unique = true)
    private String codeDept;

    @NotNull
    @Column(name = "libelle_long", nullable = false)
    private String libelleLong;

    @NotNull
    @Column(name = "chef", nullable = false)
    private String chef;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type_dept", nullable = false)
    private EnumTypeDept typeDept;

    @OneToMany(mappedBy = "departement")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Formation> departements = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodeDept() {
        return codeDept;
    }

    public Departement codeDept(String codeDept) {
        this.codeDept = codeDept;
        return this;
    }

    public void setCodeDept(String codeDept) {
        this.codeDept = codeDept;
    }

    public String getLibelleLong() {
        return libelleLong;
    }

    public Departement libelleLong(String libelleLong) {
        this.libelleLong = libelleLong;
        return this;
    }

    public void setLibelleLong(String libelleLong) {
        this.libelleLong = libelleLong;
    }

    public String getChef() {
        return chef;
    }

    public Departement chef(String chef) {
        this.chef = chef;
        return this;
    }

    public void setChef(String chef) {
        this.chef = chef;
    }

    public EnumTypeDept getTypeDept() {
        return typeDept;
    }

    public Departement typeDept(EnumTypeDept typeDept) {
        this.typeDept = typeDept;
        return this;
    }

    public void setTypeDept(EnumTypeDept typeDept) {
        this.typeDept = typeDept;
    }

    public Set<Formation> getDepartements() {
        return departements;
    }

    public Departement departements(Set<Formation> formations) {
        this.departements = formations;
        return this;
    }

    public Departement addDepartement(Formation formation) {
        this.departements.add(formation);
        formation.setDepartement(this);
        return this;
    }

    public Departement removeDepartement(Formation formation) {
        this.departements.remove(formation);
        formation.setDepartement(null);
        return this;
    }

    public void setDepartements(Set<Formation> formations) {
        this.departements = formations;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Departement)) {
            return false;
        }
        return id != null && id.equals(((Departement) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Departement{" +
            "id=" + getId() +
            ", codeDept='" + getCodeDept() + "'" +
            ", libelleLong='" + getLibelleLong() + "'" +
            ", chef='" + getChef() + "'" +
            ", typeDept='" + getTypeDept() + "'" +
            "}";
    }
}
