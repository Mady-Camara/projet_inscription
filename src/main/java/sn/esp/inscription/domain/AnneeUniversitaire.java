package sn.esp.inscription.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A AnneeUniversitaire.
 */
@Entity
@Table(name = "annee_universitaire")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "anneeuniversitaire")
public class AnneeUniversitaire implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "is_active")
    private Boolean isActive;

    @NotNull
    @Column(name = "libelle", nullable = false)
    private String libelle;

    @OneToMany(mappedBy = "anneeUniversitaire")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Inscription> anneeUniversitaires = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isIsActive() {
        return isActive;
    }

    public AnneeUniversitaire isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getLibelle() {
        return libelle;
    }

    public AnneeUniversitaire libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Set<Inscription> getAnneeUniversitaires() {
        return anneeUniversitaires;
    }

    public AnneeUniversitaire anneeUniversitaires(Set<Inscription> inscriptions) {
        this.anneeUniversitaires = inscriptions;
        return this;
    }

    public AnneeUniversitaire addAnneeUniversitaire(Inscription inscription) {
        this.anneeUniversitaires.add(inscription);
        inscription.setAnneeUniversitaire(this);
        return this;
    }

    public AnneeUniversitaire removeAnneeUniversitaire(Inscription inscription) {
        this.anneeUniversitaires.remove(inscription);
        inscription.setAnneeUniversitaire(null);
        return this;
    }

    public void setAnneeUniversitaires(Set<Inscription> inscriptions) {
        this.anneeUniversitaires = inscriptions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AnneeUniversitaire)) {
            return false;
        }
        return id != null && id.equals(((AnneeUniversitaire) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AnneeUniversitaire{" +
            "id=" + getId() +
            ", isActive='" + isIsActive() + "'" +
            ", libelle='" + getLibelle() + "'" +
            "}";
    }
}
