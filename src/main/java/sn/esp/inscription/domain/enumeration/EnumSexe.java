package sn.esp.inscription.domain.enumeration;

/**
 * The EnumSexe enumeration.
 */
public enum EnumSexe {
    Masculin, Feminin
}
