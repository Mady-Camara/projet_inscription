package sn.esp.inscription.web.rest;

import sn.esp.inscription.domain.Assureur;
import sn.esp.inscription.repository.AssureurRepository;
import sn.esp.inscription.repository.search.AssureurSearchRepository;
import sn.esp.inscription.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link sn.esp.inscription.domain.Assureur}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class AssureurResource {

    private final Logger log = LoggerFactory.getLogger(AssureurResource.class);

    private static final String ENTITY_NAME = "assureur";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AssureurRepository assureurRepository;

    private final AssureurSearchRepository assureurSearchRepository;

    public AssureurResource(AssureurRepository assureurRepository, AssureurSearchRepository assureurSearchRepository) {
        this.assureurRepository = assureurRepository;
        this.assureurSearchRepository = assureurSearchRepository;
    }

    /**
     * {@code POST  /assureurs} : Create a new assureur.
     *
     * @param assureur the assureur to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new assureur, or with status {@code 400 (Bad Request)} if the assureur has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/assureurs")
    public ResponseEntity<Assureur> createAssureur(@Valid @RequestBody Assureur assureur) throws URISyntaxException {
        log.debug("REST request to save Assureur : {}", assureur);
        if (assureur.getId() != null) {
            throw new BadRequestAlertException("A new assureur cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Assureur result = assureurRepository.save(assureur);
        assureurSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/assureurs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /assureurs} : Updates an existing assureur.
     *
     * @param assureur the assureur to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated assureur,
     * or with status {@code 400 (Bad Request)} if the assureur is not valid,
     * or with status {@code 500 (Internal Server Error)} if the assureur couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/assureurs")
    public ResponseEntity<Assureur> updateAssureur(@Valid @RequestBody Assureur assureur) throws URISyntaxException {
        log.debug("REST request to update Assureur : {}", assureur);
        if (assureur.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Assureur result = assureurRepository.save(assureur);
        assureurSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, assureur.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /assureurs} : get all the assureurs.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of assureurs in body.
     */
    @GetMapping("/assureurs")
    public List<Assureur> getAllAssureurs() {
        log.debug("REST request to get all Assureurs");
        return assureurRepository.findAll();
    }

    /**
     * {@code GET  /assureurs/:id} : get the "id" assureur.
     *
     * @param id the id of the assureur to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the assureur, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/assureurs/{id}")
    public ResponseEntity<Assureur> getAssureur(@PathVariable Long id) {
        log.debug("REST request to get Assureur : {}", id);
        Optional<Assureur> assureur = assureurRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(assureur);
    }

    /**
     * {@code DELETE  /assureurs/:id} : delete the "id" assureur.
     *
     * @param id the id of the assureur to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/assureurs/{id}")
    public ResponseEntity<Void> deleteAssureur(@PathVariable Long id) {
        log.debug("REST request to delete Assureur : {}", id);
        assureurRepository.deleteById(id);
        assureurSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/assureurs?query=:query} : search for the assureur corresponding
     * to the query.
     *
     * @param query the query of the assureur search.
     * @return the result of the search.
     */
    @GetMapping("/_search/assureurs")
    public List<Assureur> searchAssureurs(@RequestParam String query) {
        log.debug("REST request to search Assureurs for query {}", query);
        return StreamSupport
            .stream(assureurSearchRepository.search(queryStringQuery(query)).spliterator(), false)
        .collect(Collectors.toList());
    }
}
