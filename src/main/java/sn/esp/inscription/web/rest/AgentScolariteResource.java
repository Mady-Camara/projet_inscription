package sn.esp.inscription.web.rest;

import sn.esp.inscription.domain.AgentScolarite;
import sn.esp.inscription.repository.AgentScolariteRepository;
import sn.esp.inscription.repository.search.AgentScolariteSearchRepository;
import sn.esp.inscription.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link sn.esp.inscription.domain.AgentScolarite}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class AgentScolariteResource {

    private final Logger log = LoggerFactory.getLogger(AgentScolariteResource.class);

    private static final String ENTITY_NAME = "agentScolarite";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AgentScolariteRepository agentScolariteRepository;

    private final AgentScolariteSearchRepository agentScolariteSearchRepository;

    public AgentScolariteResource(AgentScolariteRepository agentScolariteRepository, AgentScolariteSearchRepository agentScolariteSearchRepository) {
        this.agentScolariteRepository = agentScolariteRepository;
        this.agentScolariteSearchRepository = agentScolariteSearchRepository;
    }

    /**
     * {@code POST  /agent-scolarites} : Create a new agentScolarite.
     *
     * @param agentScolarite the agentScolarite to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new agentScolarite, or with status {@code 400 (Bad Request)} if the agentScolarite has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/agent-scolarites")
    public ResponseEntity<AgentScolarite> createAgentScolarite(@Valid @RequestBody AgentScolarite agentScolarite) throws URISyntaxException {
        log.debug("REST request to save AgentScolarite : {}", agentScolarite);
        if (agentScolarite.getId() != null) {
            throw new BadRequestAlertException("A new agentScolarite cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AgentScolarite result = agentScolariteRepository.save(agentScolarite);
        agentScolariteSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/agent-scolarites/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /agent-scolarites} : Updates an existing agentScolarite.
     *
     * @param agentScolarite the agentScolarite to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated agentScolarite,
     * or with status {@code 400 (Bad Request)} if the agentScolarite is not valid,
     * or with status {@code 500 (Internal Server Error)} if the agentScolarite couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/agent-scolarites")
    public ResponseEntity<AgentScolarite> updateAgentScolarite(@Valid @RequestBody AgentScolarite agentScolarite) throws URISyntaxException {
        log.debug("REST request to update AgentScolarite : {}", agentScolarite);
        if (agentScolarite.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AgentScolarite result = agentScolariteRepository.save(agentScolarite);
        agentScolariteSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, agentScolarite.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /agent-scolarites} : get all the agentScolarites.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of agentScolarites in body.
     */
    @GetMapping("/agent-scolarites")
    public List<AgentScolarite> getAllAgentScolarites() {
        log.debug("REST request to get all AgentScolarites");
        return agentScolariteRepository.findAll();
    }

    /**
     * {@code GET  /agent-scolarites/:id} : get the "id" agentScolarite.
     *
     * @param id the id of the agentScolarite to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the agentScolarite, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/agent-scolarites/{id}")
    public ResponseEntity<AgentScolarite> getAgentScolarite(@PathVariable Long id) {
        log.debug("REST request to get AgentScolarite : {}", id);
        Optional<AgentScolarite> agentScolarite = agentScolariteRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(agentScolarite);
    }

    /**
     * {@code DELETE  /agent-scolarites/:id} : delete the "id" agentScolarite.
     *
     * @param id the id of the agentScolarite to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/agent-scolarites/{id}")
    public ResponseEntity<Void> deleteAgentScolarite(@PathVariable Long id) {
        log.debug("REST request to delete AgentScolarite : {}", id);
        agentScolariteRepository.deleteById(id);
        agentScolariteSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/agent-scolarites?query=:query} : search for the agentScolarite corresponding
     * to the query.
     *
     * @param query the query of the agentScolarite search.
     * @return the result of the search.
     */
    @GetMapping("/_search/agent-scolarites")
    public List<AgentScolarite> searchAgentScolarites(@RequestParam String query) {
        log.debug("REST request to search AgentScolarites for query {}", query);
        return StreamSupport
            .stream(agentScolariteSearchRepository.search(queryStringQuery(query)).spliterator(), false)
        .collect(Collectors.toList());
    }
}
