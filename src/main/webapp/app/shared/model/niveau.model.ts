import { IFormation } from 'app/shared/model/formation.model';
import { IInscription } from 'app/shared/model/inscription.model';

export interface INiveau {
  id?: number;
  codeNiveau?: string;
  libelleLong?: string;
  formation?: IFormation;
  niveaus?: IInscription[];
}

export class Niveau implements INiveau {
  constructor(
    public id?: number,
    public codeNiveau?: string,
    public libelleLong?: string,
    public formation?: IFormation,
    public niveaus?: IInscription[]
  ) {}
}
