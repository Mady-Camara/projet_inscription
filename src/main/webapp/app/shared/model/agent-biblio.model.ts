import { IUser } from 'app/core/user/user.model';

export interface IAgentBiblio {
  id?: number;
  matricule?: string;
  user?: IUser;
}

export class AgentBiblio implements IAgentBiblio {
  constructor(public id?: number, public matricule?: string, public user?: IUser) {}
}
