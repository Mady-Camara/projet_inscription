import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IAgentScolarite, AgentScolarite } from 'app/shared/model/agent-scolarite.model';
import { AgentScolariteService } from './agent-scolarite.service';
import { AgentScolariteComponent } from './agent-scolarite.component';
import { AgentScolariteDetailComponent } from './agent-scolarite-detail.component';
import { AgentScolariteUpdateComponent } from './agent-scolarite-update.component';

@Injectable({ providedIn: 'root' })
export class AgentScolariteResolve implements Resolve<IAgentScolarite> {
  constructor(private service: AgentScolariteService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAgentScolarite> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((agentScolarite: HttpResponse<AgentScolarite>) => {
          if (agentScolarite.body) {
            return of(agentScolarite.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new AgentScolarite());
  }
}

export const agentScolariteRoute: Routes = [
  {
    path: '',
    component: AgentScolariteComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'inscriptionEspApp.agentScolarite.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AgentScolariteDetailComponent,
    resolve: {
      agentScolarite: AgentScolariteResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'inscriptionEspApp.agentScolarite.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AgentScolariteUpdateComponent,
    resolve: {
      agentScolarite: AgentScolariteResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'inscriptionEspApp.agentScolarite.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AgentScolariteUpdateComponent,
    resolve: {
      agentScolarite: AgentScolariteResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'inscriptionEspApp.agentScolarite.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
