import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IAssureur, Assureur } from 'app/shared/model/assureur.model';
import { AssureurService } from './assureur.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'jhi-assureur-update',
  templateUrl: './assureur-update.component.html',
})
export class AssureurUpdateComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];

  editForm = this.fb.group({
    id: [],
    matricule: [null, [Validators.required]],
    user: [null, Validators.required],
  });

  constructor(
    protected assureurService: AssureurService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ assureur }) => {
      this.updateForm(assureur);

      this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));
    });
  }

  updateForm(assureur: IAssureur): void {
    this.editForm.patchValue({
      id: assureur.id,
      matricule: assureur.matricule,
      user: assureur.user,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const assureur = this.createFromForm();
    if (assureur.id !== undefined) {
      this.subscribeToSaveResponse(this.assureurService.update(assureur));
    } else {
      this.subscribeToSaveResponse(this.assureurService.create(assureur));
    }
  }

  private createFromForm(): IAssureur {
    return {
      ...new Assureur(),
      id: this.editForm.get(['id'])!.value,
      matricule: this.editForm.get(['matricule'])!.value,
      user: this.editForm.get(['user'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAssureur>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IUser): any {
    return item.id;
  }
}
