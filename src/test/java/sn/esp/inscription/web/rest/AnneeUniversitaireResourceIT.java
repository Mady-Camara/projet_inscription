package sn.esp.inscription.web.rest;

import sn.esp.inscription.InscriptionEspApp;
import sn.esp.inscription.domain.AnneeUniversitaire;
import sn.esp.inscription.domain.Inscription;
import sn.esp.inscription.repository.AnneeUniversitaireRepository;
import sn.esp.inscription.repository.search.AnneeUniversitaireSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AnneeUniversitaireResource} REST controller.
 */
@SpringBootTest(classes = InscriptionEspApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class AnneeUniversitaireResourceIT {

    private static final Boolean DEFAULT_IS_ACTIVE = false;
    private static final Boolean UPDATED_IS_ACTIVE = true;

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    @Autowired
    private AnneeUniversitaireRepository anneeUniversitaireRepository;

    /**
     * This repository is mocked in the sn.esp.inscription.repository.search test package.
     *
     * @see sn.esp.inscription.repository.search.AnneeUniversitaireSearchRepositoryMockConfiguration
     */
    @Autowired
    private AnneeUniversitaireSearchRepository mockAnneeUniversitaireSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAnneeUniversitaireMockMvc;

    private AnneeUniversitaire anneeUniversitaire;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AnneeUniversitaire createEntity(EntityManager em) {
        AnneeUniversitaire anneeUniversitaire = new AnneeUniversitaire()
            .isActive(DEFAULT_IS_ACTIVE)
            .libelle(DEFAULT_LIBELLE);
        // Add required entity
        Inscription inscription;
        if (TestUtil.findAll(em, Inscription.class).isEmpty()) {
            inscription = InscriptionResourceIT.createEntity(em);
            em.persist(inscription);
            em.flush();
        } else {
            inscription = TestUtil.findAll(em, Inscription.class).get(0);
        }
        anneeUniversitaire.getAnneeUniversitaires().add(inscription);
        return anneeUniversitaire;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AnneeUniversitaire createUpdatedEntity(EntityManager em) {
        AnneeUniversitaire anneeUniversitaire = new AnneeUniversitaire()
            .isActive(UPDATED_IS_ACTIVE)
            .libelle(UPDATED_LIBELLE);
        // Add required entity
        Inscription inscription;
        if (TestUtil.findAll(em, Inscription.class).isEmpty()) {
            inscription = InscriptionResourceIT.createUpdatedEntity(em);
            em.persist(inscription);
            em.flush();
        } else {
            inscription = TestUtil.findAll(em, Inscription.class).get(0);
        }
        anneeUniversitaire.getAnneeUniversitaires().add(inscription);
        return anneeUniversitaire;
    }

    @BeforeEach
    public void initTest() {
        anneeUniversitaire = createEntity(em);
    }

    @Test
    @Transactional
    public void createAnneeUniversitaire() throws Exception {
        int databaseSizeBeforeCreate = anneeUniversitaireRepository.findAll().size();
        // Create the AnneeUniversitaire
        restAnneeUniversitaireMockMvc.perform(post("/api/annee-universitaires").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(anneeUniversitaire)))
            .andExpect(status().isCreated());

        // Validate the AnneeUniversitaire in the database
        List<AnneeUniversitaire> anneeUniversitaireList = anneeUniversitaireRepository.findAll();
        assertThat(anneeUniversitaireList).hasSize(databaseSizeBeforeCreate + 1);
        AnneeUniversitaire testAnneeUniversitaire = anneeUniversitaireList.get(anneeUniversitaireList.size() - 1);
        assertThat(testAnneeUniversitaire.isIsActive()).isEqualTo(DEFAULT_IS_ACTIVE);
        assertThat(testAnneeUniversitaire.getLibelle()).isEqualTo(DEFAULT_LIBELLE);

        // Validate the AnneeUniversitaire in Elasticsearch
        verify(mockAnneeUniversitaireSearchRepository, times(1)).save(testAnneeUniversitaire);
    }

    @Test
    @Transactional
    public void createAnneeUniversitaireWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = anneeUniversitaireRepository.findAll().size();

        // Create the AnneeUniversitaire with an existing ID
        anneeUniversitaire.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAnneeUniversitaireMockMvc.perform(post("/api/annee-universitaires").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(anneeUniversitaire)))
            .andExpect(status().isBadRequest());

        // Validate the AnneeUniversitaire in the database
        List<AnneeUniversitaire> anneeUniversitaireList = anneeUniversitaireRepository.findAll();
        assertThat(anneeUniversitaireList).hasSize(databaseSizeBeforeCreate);

        // Validate the AnneeUniversitaire in Elasticsearch
        verify(mockAnneeUniversitaireSearchRepository, times(0)).save(anneeUniversitaire);
    }


    @Test
    @Transactional
    public void checkLibelleIsRequired() throws Exception {
        int databaseSizeBeforeTest = anneeUniversitaireRepository.findAll().size();
        // set the field null
        anneeUniversitaire.setLibelle(null);

        // Create the AnneeUniversitaire, which fails.


        restAnneeUniversitaireMockMvc.perform(post("/api/annee-universitaires").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(anneeUniversitaire)))
            .andExpect(status().isBadRequest());

        List<AnneeUniversitaire> anneeUniversitaireList = anneeUniversitaireRepository.findAll();
        assertThat(anneeUniversitaireList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAnneeUniversitaires() throws Exception {
        // Initialize the database
        anneeUniversitaireRepository.saveAndFlush(anneeUniversitaire);

        // Get all the anneeUniversitaireList
        restAnneeUniversitaireMockMvc.perform(get("/api/annee-universitaires?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(anneeUniversitaire.getId().intValue())))
            .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)));
    }
    
    @Test
    @Transactional
    public void getAnneeUniversitaire() throws Exception {
        // Initialize the database
        anneeUniversitaireRepository.saveAndFlush(anneeUniversitaire);

        // Get the anneeUniversitaire
        restAnneeUniversitaireMockMvc.perform(get("/api/annee-universitaires/{id}", anneeUniversitaire.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(anneeUniversitaire.getId().intValue()))
            .andExpect(jsonPath("$.isActive").value(DEFAULT_IS_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE));
    }
    @Test
    @Transactional
    public void getNonExistingAnneeUniversitaire() throws Exception {
        // Get the anneeUniversitaire
        restAnneeUniversitaireMockMvc.perform(get("/api/annee-universitaires/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAnneeUniversitaire() throws Exception {
        // Initialize the database
        anneeUniversitaireRepository.saveAndFlush(anneeUniversitaire);

        int databaseSizeBeforeUpdate = anneeUniversitaireRepository.findAll().size();

        // Update the anneeUniversitaire
        AnneeUniversitaire updatedAnneeUniversitaire = anneeUniversitaireRepository.findById(anneeUniversitaire.getId()).get();
        // Disconnect from session so that the updates on updatedAnneeUniversitaire are not directly saved in db
        em.detach(updatedAnneeUniversitaire);
        updatedAnneeUniversitaire
            .isActive(UPDATED_IS_ACTIVE)
            .libelle(UPDATED_LIBELLE);

        restAnneeUniversitaireMockMvc.perform(put("/api/annee-universitaires").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAnneeUniversitaire)))
            .andExpect(status().isOk());

        // Validate the AnneeUniversitaire in the database
        List<AnneeUniversitaire> anneeUniversitaireList = anneeUniversitaireRepository.findAll();
        assertThat(anneeUniversitaireList).hasSize(databaseSizeBeforeUpdate);
        AnneeUniversitaire testAnneeUniversitaire = anneeUniversitaireList.get(anneeUniversitaireList.size() - 1);
        assertThat(testAnneeUniversitaire.isIsActive()).isEqualTo(UPDATED_IS_ACTIVE);
        assertThat(testAnneeUniversitaire.getLibelle()).isEqualTo(UPDATED_LIBELLE);

        // Validate the AnneeUniversitaire in Elasticsearch
        verify(mockAnneeUniversitaireSearchRepository, times(1)).save(testAnneeUniversitaire);
    }

    @Test
    @Transactional
    public void updateNonExistingAnneeUniversitaire() throws Exception {
        int databaseSizeBeforeUpdate = anneeUniversitaireRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAnneeUniversitaireMockMvc.perform(put("/api/annee-universitaires").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(anneeUniversitaire)))
            .andExpect(status().isBadRequest());

        // Validate the AnneeUniversitaire in the database
        List<AnneeUniversitaire> anneeUniversitaireList = anneeUniversitaireRepository.findAll();
        assertThat(anneeUniversitaireList).hasSize(databaseSizeBeforeUpdate);

        // Validate the AnneeUniversitaire in Elasticsearch
        verify(mockAnneeUniversitaireSearchRepository, times(0)).save(anneeUniversitaire);
    }

    @Test
    @Transactional
    public void deleteAnneeUniversitaire() throws Exception {
        // Initialize the database
        anneeUniversitaireRepository.saveAndFlush(anneeUniversitaire);

        int databaseSizeBeforeDelete = anneeUniversitaireRepository.findAll().size();

        // Delete the anneeUniversitaire
        restAnneeUniversitaireMockMvc.perform(delete("/api/annee-universitaires/{id}", anneeUniversitaire.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AnneeUniversitaire> anneeUniversitaireList = anneeUniversitaireRepository.findAll();
        assertThat(anneeUniversitaireList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the AnneeUniversitaire in Elasticsearch
        verify(mockAnneeUniversitaireSearchRepository, times(1)).deleteById(anneeUniversitaire.getId());
    }

    @Test
    @Transactional
    public void searchAnneeUniversitaire() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        anneeUniversitaireRepository.saveAndFlush(anneeUniversitaire);
        when(mockAnneeUniversitaireSearchRepository.search(queryStringQuery("id:" + anneeUniversitaire.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(anneeUniversitaire), PageRequest.of(0, 1), 1));

        // Search the anneeUniversitaire
        restAnneeUniversitaireMockMvc.perform(get("/api/_search/annee-universitaires?query=id:" + anneeUniversitaire.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(anneeUniversitaire.getId().intValue())))
            .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)));
    }
}
