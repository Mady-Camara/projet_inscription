package sn.esp.inscription.web.rest;

import sn.esp.inscription.InscriptionEspApp;
import sn.esp.inscription.domain.Departement;
import sn.esp.inscription.domain.Formation;
import sn.esp.inscription.repository.DepartementRepository;
import sn.esp.inscription.repository.search.DepartementSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import sn.esp.inscription.domain.enumeration.EnumTypeDept;
/**
 * Integration tests for the {@link DepartementResource} REST controller.
 */
@SpringBootTest(classes = InscriptionEspApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class DepartementResourceIT {

    private static final String DEFAULT_CODE_DEPT = "AAAAAAAAAA";
    private static final String UPDATED_CODE_DEPT = "BBBBBBBBBB";

    private static final String DEFAULT_LIBELLE_LONG = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE_LONG = "BBBBBBBBBB";

    private static final String DEFAULT_CHEF = "AAAAAAAAAA";
    private static final String UPDATED_CHEF = "BBBBBBBBBB";

    private static final EnumTypeDept DEFAULT_TYPE_DEPT = EnumTypeDept.Departement;
    private static final EnumTypeDept UPDATED_TYPE_DEPT = EnumTypeDept.Service;

    @Autowired
    private DepartementRepository departementRepository;

    /**
     * This repository is mocked in the sn.esp.inscription.repository.search test package.
     *
     * @see sn.esp.inscription.repository.search.DepartementSearchRepositoryMockConfiguration
     */
    @Autowired
    private DepartementSearchRepository mockDepartementSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDepartementMockMvc;

    private Departement departement;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Departement createEntity(EntityManager em) {
        Departement departement = new Departement()
            .codeDept(DEFAULT_CODE_DEPT)
            .libelleLong(DEFAULT_LIBELLE_LONG)
            .chef(DEFAULT_CHEF)
            .typeDept(DEFAULT_TYPE_DEPT);
        // Add required entity
        Formation formation;
        if (TestUtil.findAll(em, Formation.class).isEmpty()) {
            formation = FormationResourceIT.createEntity(em);
            em.persist(formation);
            em.flush();
        } else {
            formation = TestUtil.findAll(em, Formation.class).get(0);
        }
        departement.getDepartements().add(formation);
        return departement;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Departement createUpdatedEntity(EntityManager em) {
        Departement departement = new Departement()
            .codeDept(UPDATED_CODE_DEPT)
            .libelleLong(UPDATED_LIBELLE_LONG)
            .chef(UPDATED_CHEF)
            .typeDept(UPDATED_TYPE_DEPT);
        // Add required entity
        Formation formation;
        if (TestUtil.findAll(em, Formation.class).isEmpty()) {
            formation = FormationResourceIT.createUpdatedEntity(em);
            em.persist(formation);
            em.flush();
        } else {
            formation = TestUtil.findAll(em, Formation.class).get(0);
        }
        departement.getDepartements().add(formation);
        return departement;
    }

    @BeforeEach
    public void initTest() {
        departement = createEntity(em);
    }

    @Test
    @Transactional
    public void createDepartement() throws Exception {
        int databaseSizeBeforeCreate = departementRepository.findAll().size();
        // Create the Departement
        restDepartementMockMvc.perform(post("/api/departements").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(departement)))
            .andExpect(status().isCreated());

        // Validate the Departement in the database
        List<Departement> departementList = departementRepository.findAll();
        assertThat(departementList).hasSize(databaseSizeBeforeCreate + 1);
        Departement testDepartement = departementList.get(departementList.size() - 1);
        assertThat(testDepartement.getCodeDept()).isEqualTo(DEFAULT_CODE_DEPT);
        assertThat(testDepartement.getLibelleLong()).isEqualTo(DEFAULT_LIBELLE_LONG);
        assertThat(testDepartement.getChef()).isEqualTo(DEFAULT_CHEF);
        assertThat(testDepartement.getTypeDept()).isEqualTo(DEFAULT_TYPE_DEPT);

        // Validate the Departement in Elasticsearch
        verify(mockDepartementSearchRepository, times(1)).save(testDepartement);
    }

    @Test
    @Transactional
    public void createDepartementWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = departementRepository.findAll().size();

        // Create the Departement with an existing ID
        departement.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDepartementMockMvc.perform(post("/api/departements").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(departement)))
            .andExpect(status().isBadRequest());

        // Validate the Departement in the database
        List<Departement> departementList = departementRepository.findAll();
        assertThat(departementList).hasSize(databaseSizeBeforeCreate);

        // Validate the Departement in Elasticsearch
        verify(mockDepartementSearchRepository, times(0)).save(departement);
    }


    @Test
    @Transactional
    public void checkCodeDeptIsRequired() throws Exception {
        int databaseSizeBeforeTest = departementRepository.findAll().size();
        // set the field null
        departement.setCodeDept(null);

        // Create the Departement, which fails.


        restDepartementMockMvc.perform(post("/api/departements").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(departement)))
            .andExpect(status().isBadRequest());

        List<Departement> departementList = departementRepository.findAll();
        assertThat(departementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLibelleLongIsRequired() throws Exception {
        int databaseSizeBeforeTest = departementRepository.findAll().size();
        // set the field null
        departement.setLibelleLong(null);

        // Create the Departement, which fails.


        restDepartementMockMvc.perform(post("/api/departements").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(departement)))
            .andExpect(status().isBadRequest());

        List<Departement> departementList = departementRepository.findAll();
        assertThat(departementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkChefIsRequired() throws Exception {
        int databaseSizeBeforeTest = departementRepository.findAll().size();
        // set the field null
        departement.setChef(null);

        // Create the Departement, which fails.


        restDepartementMockMvc.perform(post("/api/departements").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(departement)))
            .andExpect(status().isBadRequest());

        List<Departement> departementList = departementRepository.findAll();
        assertThat(departementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTypeDeptIsRequired() throws Exception {
        int databaseSizeBeforeTest = departementRepository.findAll().size();
        // set the field null
        departement.setTypeDept(null);

        // Create the Departement, which fails.


        restDepartementMockMvc.perform(post("/api/departements").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(departement)))
            .andExpect(status().isBadRequest());

        List<Departement> departementList = departementRepository.findAll();
        assertThat(departementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDepartements() throws Exception {
        // Initialize the database
        departementRepository.saveAndFlush(departement);

        // Get all the departementList
        restDepartementMockMvc.perform(get("/api/departements?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(departement.getId().intValue())))
            .andExpect(jsonPath("$.[*].codeDept").value(hasItem(DEFAULT_CODE_DEPT)))
            .andExpect(jsonPath("$.[*].libelleLong").value(hasItem(DEFAULT_LIBELLE_LONG)))
            .andExpect(jsonPath("$.[*].chef").value(hasItem(DEFAULT_CHEF)))
            .andExpect(jsonPath("$.[*].typeDept").value(hasItem(DEFAULT_TYPE_DEPT.toString())));
    }
    
    @Test
    @Transactional
    public void getDepartement() throws Exception {
        // Initialize the database
        departementRepository.saveAndFlush(departement);

        // Get the departement
        restDepartementMockMvc.perform(get("/api/departements/{id}", departement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(departement.getId().intValue()))
            .andExpect(jsonPath("$.codeDept").value(DEFAULT_CODE_DEPT))
            .andExpect(jsonPath("$.libelleLong").value(DEFAULT_LIBELLE_LONG))
            .andExpect(jsonPath("$.chef").value(DEFAULT_CHEF))
            .andExpect(jsonPath("$.typeDept").value(DEFAULT_TYPE_DEPT.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingDepartement() throws Exception {
        // Get the departement
        restDepartementMockMvc.perform(get("/api/departements/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDepartement() throws Exception {
        // Initialize the database
        departementRepository.saveAndFlush(departement);

        int databaseSizeBeforeUpdate = departementRepository.findAll().size();

        // Update the departement
        Departement updatedDepartement = departementRepository.findById(departement.getId()).get();
        // Disconnect from session so that the updates on updatedDepartement are not directly saved in db
        em.detach(updatedDepartement);
        updatedDepartement
            .codeDept(UPDATED_CODE_DEPT)
            .libelleLong(UPDATED_LIBELLE_LONG)
            .chef(UPDATED_CHEF)
            .typeDept(UPDATED_TYPE_DEPT);

        restDepartementMockMvc.perform(put("/api/departements").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedDepartement)))
            .andExpect(status().isOk());

        // Validate the Departement in the database
        List<Departement> departementList = departementRepository.findAll();
        assertThat(departementList).hasSize(databaseSizeBeforeUpdate);
        Departement testDepartement = departementList.get(departementList.size() - 1);
        assertThat(testDepartement.getCodeDept()).isEqualTo(UPDATED_CODE_DEPT);
        assertThat(testDepartement.getLibelleLong()).isEqualTo(UPDATED_LIBELLE_LONG);
        assertThat(testDepartement.getChef()).isEqualTo(UPDATED_CHEF);
        assertThat(testDepartement.getTypeDept()).isEqualTo(UPDATED_TYPE_DEPT);

        // Validate the Departement in Elasticsearch
        verify(mockDepartementSearchRepository, times(1)).save(testDepartement);
    }

    @Test
    @Transactional
    public void updateNonExistingDepartement() throws Exception {
        int databaseSizeBeforeUpdate = departementRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDepartementMockMvc.perform(put("/api/departements").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(departement)))
            .andExpect(status().isBadRequest());

        // Validate the Departement in the database
        List<Departement> departementList = departementRepository.findAll();
        assertThat(departementList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Departement in Elasticsearch
        verify(mockDepartementSearchRepository, times(0)).save(departement);
    }

    @Test
    @Transactional
    public void deleteDepartement() throws Exception {
        // Initialize the database
        departementRepository.saveAndFlush(departement);

        int databaseSizeBeforeDelete = departementRepository.findAll().size();

        // Delete the departement
        restDepartementMockMvc.perform(delete("/api/departements/{id}", departement.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Departement> departementList = departementRepository.findAll();
        assertThat(departementList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Departement in Elasticsearch
        verify(mockDepartementSearchRepository, times(1)).deleteById(departement.getId());
    }

    @Test
    @Transactional
    public void searchDepartement() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        departementRepository.saveAndFlush(departement);
        when(mockDepartementSearchRepository.search(queryStringQuery("id:" + departement.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(departement), PageRequest.of(0, 1), 1));

        // Search the departement
        restDepartementMockMvc.perform(get("/api/_search/departements?query=id:" + departement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(departement.getId().intValue())))
            .andExpect(jsonPath("$.[*].codeDept").value(hasItem(DEFAULT_CODE_DEPT)))
            .andExpect(jsonPath("$.[*].libelleLong").value(hasItem(DEFAULT_LIBELLE_LONG)))
            .andExpect(jsonPath("$.[*].chef").value(hasItem(DEFAULT_CHEF)))
            .andExpect(jsonPath("$.[*].typeDept").value(hasItem(DEFAULT_TYPE_DEPT.toString())));
    }
}
