package sn.esp.inscription.web.rest;

import sn.esp.inscription.InscriptionEspApp;
import sn.esp.inscription.domain.Assureur;
import sn.esp.inscription.domain.User;
import sn.esp.inscription.repository.AssureurRepository;
import sn.esp.inscription.repository.search.AssureurSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AssureurResource} REST controller.
 */
@SpringBootTest(classes = InscriptionEspApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class AssureurResourceIT {

    private static final String DEFAULT_MATRICULE = "AAAAAAAAAA";
    private static final String UPDATED_MATRICULE = "BBBBBBBBBB";

    @Autowired
    private AssureurRepository assureurRepository;

    /**
     * This repository is mocked in the sn.esp.inscription.repository.search test package.
     *
     * @see sn.esp.inscription.repository.search.AssureurSearchRepositoryMockConfiguration
     */
    @Autowired
    private AssureurSearchRepository mockAssureurSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAssureurMockMvc;

    private Assureur assureur;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Assureur createEntity(EntityManager em) {
        Assureur assureur = new Assureur()
            .matricule(DEFAULT_MATRICULE);
        // Add required entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        assureur.setUser(user);
        return assureur;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Assureur createUpdatedEntity(EntityManager em) {
        Assureur assureur = new Assureur()
            .matricule(UPDATED_MATRICULE);
        // Add required entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        assureur.setUser(user);
        return assureur;
    }

    @BeforeEach
    public void initTest() {
        assureur = createEntity(em);
    }

    @Test
    @Transactional
    public void createAssureur() throws Exception {
        int databaseSizeBeforeCreate = assureurRepository.findAll().size();
        // Create the Assureur
        restAssureurMockMvc.perform(post("/api/assureurs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assureur)))
            .andExpect(status().isCreated());

        // Validate the Assureur in the database
        List<Assureur> assureurList = assureurRepository.findAll();
        assertThat(assureurList).hasSize(databaseSizeBeforeCreate + 1);
        Assureur testAssureur = assureurList.get(assureurList.size() - 1);
        assertThat(testAssureur.getMatricule()).isEqualTo(DEFAULT_MATRICULE);

        // Validate the Assureur in Elasticsearch
        verify(mockAssureurSearchRepository, times(1)).save(testAssureur);
    }

    @Test
    @Transactional
    public void createAssureurWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = assureurRepository.findAll().size();

        // Create the Assureur with an existing ID
        assureur.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAssureurMockMvc.perform(post("/api/assureurs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assureur)))
            .andExpect(status().isBadRequest());

        // Validate the Assureur in the database
        List<Assureur> assureurList = assureurRepository.findAll();
        assertThat(assureurList).hasSize(databaseSizeBeforeCreate);

        // Validate the Assureur in Elasticsearch
        verify(mockAssureurSearchRepository, times(0)).save(assureur);
    }


    @Test
    @Transactional
    public void checkMatriculeIsRequired() throws Exception {
        int databaseSizeBeforeTest = assureurRepository.findAll().size();
        // set the field null
        assureur.setMatricule(null);

        // Create the Assureur, which fails.


        restAssureurMockMvc.perform(post("/api/assureurs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assureur)))
            .andExpect(status().isBadRequest());

        List<Assureur> assureurList = assureurRepository.findAll();
        assertThat(assureurList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAssureurs() throws Exception {
        // Initialize the database
        assureurRepository.saveAndFlush(assureur);

        // Get all the assureurList
        restAssureurMockMvc.perform(get("/api/assureurs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(assureur.getId().intValue())))
            .andExpect(jsonPath("$.[*].matricule").value(hasItem(DEFAULT_MATRICULE)));
    }
    
    @Test
    @Transactional
    public void getAssureur() throws Exception {
        // Initialize the database
        assureurRepository.saveAndFlush(assureur);

        // Get the assureur
        restAssureurMockMvc.perform(get("/api/assureurs/{id}", assureur.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(assureur.getId().intValue()))
            .andExpect(jsonPath("$.matricule").value(DEFAULT_MATRICULE));
    }
    @Test
    @Transactional
    public void getNonExistingAssureur() throws Exception {
        // Get the assureur
        restAssureurMockMvc.perform(get("/api/assureurs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAssureur() throws Exception {
        // Initialize the database
        assureurRepository.saveAndFlush(assureur);

        int databaseSizeBeforeUpdate = assureurRepository.findAll().size();

        // Update the assureur
        Assureur updatedAssureur = assureurRepository.findById(assureur.getId()).get();
        // Disconnect from session so that the updates on updatedAssureur are not directly saved in db
        em.detach(updatedAssureur);
        updatedAssureur
            .matricule(UPDATED_MATRICULE);

        restAssureurMockMvc.perform(put("/api/assureurs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAssureur)))
            .andExpect(status().isOk());

        // Validate the Assureur in the database
        List<Assureur> assureurList = assureurRepository.findAll();
        assertThat(assureurList).hasSize(databaseSizeBeforeUpdate);
        Assureur testAssureur = assureurList.get(assureurList.size() - 1);
        assertThat(testAssureur.getMatricule()).isEqualTo(UPDATED_MATRICULE);

        // Validate the Assureur in Elasticsearch
        verify(mockAssureurSearchRepository, times(1)).save(testAssureur);
    }

    @Test
    @Transactional
    public void updateNonExistingAssureur() throws Exception {
        int databaseSizeBeforeUpdate = assureurRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAssureurMockMvc.perform(put("/api/assureurs").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assureur)))
            .andExpect(status().isBadRequest());

        // Validate the Assureur in the database
        List<Assureur> assureurList = assureurRepository.findAll();
        assertThat(assureurList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Assureur in Elasticsearch
        verify(mockAssureurSearchRepository, times(0)).save(assureur);
    }

    @Test
    @Transactional
    public void deleteAssureur() throws Exception {
        // Initialize the database
        assureurRepository.saveAndFlush(assureur);

        int databaseSizeBeforeDelete = assureurRepository.findAll().size();

        // Delete the assureur
        restAssureurMockMvc.perform(delete("/api/assureurs/{id}", assureur.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Assureur> assureurList = assureurRepository.findAll();
        assertThat(assureurList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Assureur in Elasticsearch
        verify(mockAssureurSearchRepository, times(1)).deleteById(assureur.getId());
    }

    @Test
    @Transactional
    public void searchAssureur() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        assureurRepository.saveAndFlush(assureur);
        when(mockAssureurSearchRepository.search(queryStringQuery("id:" + assureur.getId())))
            .thenReturn(Collections.singletonList(assureur));

        // Search the assureur
        restAssureurMockMvc.perform(get("/api/_search/assureurs?query=id:" + assureur.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(assureur.getId().intValue())))
            .andExpect(jsonPath("$.[*].matricule").value(hasItem(DEFAULT_MATRICULE)));
    }
}
