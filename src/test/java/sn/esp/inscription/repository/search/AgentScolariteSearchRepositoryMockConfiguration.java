package sn.esp.inscription.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link AgentScolariteSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class AgentScolariteSearchRepositoryMockConfiguration {

    @MockBean
    private AgentScolariteSearchRepository mockAgentScolariteSearchRepository;

}
