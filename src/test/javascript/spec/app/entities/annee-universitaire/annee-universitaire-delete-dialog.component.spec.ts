import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { InscriptionEspTestModule } from '../../../test.module';
import { MockEventManager } from '../../../helpers/mock-event-manager.service';
import { MockActiveModal } from '../../../helpers/mock-active-modal.service';
import { AnneeUniversitaireDeleteDialogComponent } from 'app/entities/annee-universitaire/annee-universitaire-delete-dialog.component';
import { AnneeUniversitaireService } from 'app/entities/annee-universitaire/annee-universitaire.service';

describe('Component Tests', () => {
  describe('AnneeUniversitaire Management Delete Component', () => {
    let comp: AnneeUniversitaireDeleteDialogComponent;
    let fixture: ComponentFixture<AnneeUniversitaireDeleteDialogComponent>;
    let service: AnneeUniversitaireService;
    let mockEventManager: MockEventManager;
    let mockActiveModal: MockActiveModal;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [InscriptionEspTestModule],
        declarations: [AnneeUniversitaireDeleteDialogComponent],
      })
        .overrideTemplate(AnneeUniversitaireDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AnneeUniversitaireDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AnneeUniversitaireService);
      mockEventManager = TestBed.get(JhiEventManager);
      mockActiveModal = TestBed.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.closeSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));

      it('Should not call delete service on clear', () => {
        // GIVEN
        spyOn(service, 'delete');

        // WHEN
        comp.cancel();

        // THEN
        expect(service.delete).not.toHaveBeenCalled();
        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
      });
    });
  });
});
