import { browser, ExpectedConditions as ec /* , promise */ } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  EtudiantComponentsPage,
  /* EtudiantDeleteDialog, */
  EtudiantUpdatePage,
} from './etudiant.page-object';

const expect = chai.expect;

describe('Etudiant e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let etudiantComponentsPage: EtudiantComponentsPage;
  let etudiantUpdatePage: EtudiantUpdatePage;
  /* let etudiantDeleteDialog: EtudiantDeleteDialog; */

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Etudiants', async () => {
    await navBarPage.goToEntity('etudiant');
    etudiantComponentsPage = new EtudiantComponentsPage();
    await browser.wait(ec.visibilityOf(etudiantComponentsPage.title), 5000);
    expect(await etudiantComponentsPage.getTitle()).to.eq('inscriptionEspApp.etudiant.home.title');
    await browser.wait(ec.or(ec.visibilityOf(etudiantComponentsPage.entities), ec.visibilityOf(etudiantComponentsPage.noResult)), 1000);
  });

  it('should load create Etudiant page', async () => {
    await etudiantComponentsPage.clickOnCreateButton();
    etudiantUpdatePage = new EtudiantUpdatePage();
    expect(await etudiantUpdatePage.getPageTitle()).to.eq('inscriptionEspApp.etudiant.home.createOrEditLabel');
    await etudiantUpdatePage.cancel();
  });

  /* it('should create and save Etudiants', async () => {
        const nbButtonsBeforeCreate = await etudiantComponentsPage.countDeleteButtons();

        await etudiantComponentsPage.clickOnCreateButton();

        await promise.all([
            etudiantUpdatePage.setNumIdentifiantInput('numIdentifiant'),
            etudiantUpdatePage.setDateNaissanceInput('2000-12-31'),
            etudiantUpdatePage.setLieuNaissanceInput('lieuNaissance'),
            etudiantUpdatePage.sexeSelectLastOption(),
            etudiantUpdatePage.setIneInput('ine'),
            etudiantUpdatePage.setTelephoneInput('telephone'),
            etudiantUpdatePage.userSelectLastOption(),
        ]);

        expect(await etudiantUpdatePage.getNumIdentifiantInput()).to.eq('numIdentifiant', 'Expected NumIdentifiant value to be equals to numIdentifiant');
        expect(await etudiantUpdatePage.getDateNaissanceInput()).to.eq('2000-12-31', 'Expected dateNaissance value to be equals to 2000-12-31');
        expect(await etudiantUpdatePage.getLieuNaissanceInput()).to.eq('lieuNaissance', 'Expected LieuNaissance value to be equals to lieuNaissance');
        expect(await etudiantUpdatePage.getIneInput()).to.eq('ine', 'Expected Ine value to be equals to ine');
        expect(await etudiantUpdatePage.getTelephoneInput()).to.eq('telephone', 'Expected Telephone value to be equals to telephone');

        await etudiantUpdatePage.save();
        expect(await etudiantUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await etudiantComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    }); */

  /* it('should delete last Etudiant', async () => {
        const nbButtonsBeforeDelete = await etudiantComponentsPage.countDeleteButtons();
        await etudiantComponentsPage.clickOnLastDeleteButton();

        etudiantDeleteDialog = new EtudiantDeleteDialog();
        expect(await etudiantDeleteDialog.getDialogTitle())
            .to.eq('inscriptionEspApp.etudiant.delete.question');
        await etudiantDeleteDialog.clickOnConfirmButton();

        expect(await etudiantComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    }); */

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
