import { element, by, ElementFinder } from 'protractor';

export class NiveauComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-niveau div table .btn-danger'));
  title = element.all(by.css('jhi-niveau div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class NiveauUpdatePage {
  pageTitle = element(by.id('jhi-niveau-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  codeNiveauInput = element(by.id('field_codeNiveau'));
  libelleLongInput = element(by.id('field_libelleLong'));

  formationSelect = element(by.id('field_formation'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setCodeNiveauInput(codeNiveau: string): Promise<void> {
    await this.codeNiveauInput.sendKeys(codeNiveau);
  }

  async getCodeNiveauInput(): Promise<string> {
    return await this.codeNiveauInput.getAttribute('value');
  }

  async setLibelleLongInput(libelleLong: string): Promise<void> {
    await this.libelleLongInput.sendKeys(libelleLong);
  }

  async getLibelleLongInput(): Promise<string> {
    return await this.libelleLongInput.getAttribute('value');
  }

  async formationSelectLastOption(): Promise<void> {
    await this.formationSelect.all(by.tagName('option')).last().click();
  }

  async formationSelectOption(option: string): Promise<void> {
    await this.formationSelect.sendKeys(option);
  }

  getFormationSelect(): ElementFinder {
    return this.formationSelect;
  }

  async getFormationSelectedOption(): Promise<string> {
    return await this.formationSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class NiveauDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-niveau-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-niveau'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
